/*
 * file: complex.cpp
 * This is the implementation of our complex number
 * class whose interface resides in the file complex.h
 */

#include "complex.h"

#include <cmath> // for the square root function needed for norm()

complex::complex(double re, double im)
{
	//initialize the real and imaginary parts:
	real = re;
	imag = im;
}

complex::complex(const complex& z)
{
	//we need to make *this a copy of z:
	real = z.real;
	imag = z.imag;
}

ostream& operator<<(ostream& o, const complex& z)
{
	o << "(" << z.real;
	if(z.imag>=0)
		o << " + " << z.imag;
	else
		o << " - " << -z.imag;
	o << "i)";
	return o;
}

istream& operator>>(istream& i, complex& z)
{
	return (i>> z.real >> z.imag);
}

complex complex::conj()
{
	complex temp;
	temp.real = real;
	temp.imag = -imag;
	return temp;
	/* NOTE: alternatively, you could use a constructor to make a
	 * new complex number with the right real and imaginary parts,
	 * and return it straight away: */
	// return complex(real,-imag);
}

complex::complex()
{
	real = 0;
	imag = 0;
}

double complex::norm()
{
	/* TODO: write this */
	double n = sqrt((real*real)+(imag*imag));
	return n;
}

complex operator+(const complex& w, const complex& z)
{
	complex retVal;
	retVal.real = w.real + z.real;
	retVal.imag = w.imag + z.imag;
	return retVal;
	/* again, this could also be written as: */
	// return complex(w.real+z.real,w.imag+z.imag);
}

complex operator-(const complex& w)
{
	/* TODO: write this */
	real = -real; //negation
	imag = -imag;
	/* NOTE: this is unary negation, not subtraction. */
}

complex operator-(const complex& w, const complex& z)
{
	/* TODO: write this */
	complex total; //total is the resultant, declaring complex # that stores subtraction
	total.real = w.real - z.real; //real of total will store subtraction of w and z (real parts)
	total.imag = w.imag - z.imag; //imag of total will store subtraction of w and z (imaginary parts)
	return total;
}

complex operator*(const complex& w, const complex& z)
{
	/* TODO: write this */
	complex total; //total is the resultant, declaring complex number that stores product
	total.real = (w.real * z.real) - (w.imag * z.imag); //real will store real part of product
	total.imag = (w.real * z.imag) + (w.imag * z.real); //imag will store imaginary part of the product
	return total;
}

complex operator/(complex& w, complex& z)
{
	/* TODO: write this */
	complex total; //complex number that stores quotient
	complex numerator; //storing numerator of division
	complex denominator; //stores denominator

	numerator = w * z.conj(); //numerator is multiplied by the conjugate of the denominator
	denominator = z * z.conj(); //denominator multiplied by its conjugate

	total.real = numerator.real / denominator.real; //real part of total stored
	total.imag = denominator.imag / denominator.imag; //imaginary part of the total stored
	return total;

}

complex operator^(const complex& w, int a)
{
	/* NOTE: there are much more efficient ways to do exponentiation,
	 * but it won't make much difference for our application. */
	complex retVal(1,0); //initialize it to 1
	for(int i=0; i<a; i++)
		retVal = retVal*w;
	return retVal;
}
